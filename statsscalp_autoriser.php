<?php

/**
 * Déclaration d'autorisations
 *
 * @plugin Statistiques scalp pour SPIP
 * @license GNU/GPL
 * @package SPIP\Stats\Pipelines
 **/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction du pipeline autoriser. N'a rien à faire
 *
 * @pipeline autoriser
 */
function statsscalp_autoriser() {
}

/**
 * Autoriser l'affichage du menu de statistiques
 *
 * @uses autoriser_statsscalp_dist()
 * @param  string $faire Action demandée
 * @param  string $type Type d'objet sur lequel appliquer l'action
 * @param  int $id Identifiant de l'objet
 * @param  array $qui Description de l'auteur demandant l'autorisation
 * @param  array $opt Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 */
function autoriser_statsscalp_menu_dist($faire, $type = '', $id = 0, $qui = null, $opt = null) {
	return autoriser('voirstats', $type, $id, $qui, $opt);
}


