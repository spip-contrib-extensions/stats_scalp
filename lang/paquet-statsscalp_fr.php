<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	// S
	'statsscalp_nom' => 'Statistiques scalp',
	'statsscalp_description' => 'Permet d\'effacer un jour pour retrouver des statistiques normales',
	'statsscalp_slogan' => 'Effacer les jours où les statistiques dérapent',
];
