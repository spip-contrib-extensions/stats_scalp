# Stats Scalp

![stats_scalp](./prive/themes/spip/images/statsscalp-xx.svg)

Il arrive parfois qu'un site subisse une attaque d'un bot et que les statistiques explosent sur un jour précis.

Ce mini-plugin permet de scalper (effacer) les statistiques du jour en question pour retrouver des statistiques normales.

Vous pouvez désinstaller et supprimer le plugin après utilisation.


## Documentation

https://contrib.spip.net/5522


