<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function formulaires_statsscalp_charger_dist() {
	$valeurs = [
		'jour' => '',
	];
	return $valeurs;
}

function formulaires_statsscalp_verifier_dist() {
	$erreurs = [];
	if (!_request('jour')) {
		$erreurs['message_erreur'] = "Indiquez un jour à effacer !";
	}

	return $erreurs;
}

function formulaires_statsscalp_traiter_dist() {

	$retours = [];
	$jour = _request('jour');
	set_request('jour', $jour);

	$date = DateTime::createFromFormat('d/m/Y', $jour);
	$dateSQL = $date->format('Y-m-d');

	// spip_visites
	sql_delete('spip_visites', 'date = ' . sql_quote($dateSQL));

	// spip_visites_articles
	sql_delete('spip_visites_articles', 'date = ' . sql_quote($dateSQL));

	$retours['message_ok'] = _T('statsscalp:jour_efface', array('jour'=> $jour));
	return $retours;
}
